﻿using UnityEngine;

// Rocket - with rigidbody
public class FactoryRocket : MonoBehaviour {

    [Range(0f, 2000f)]
    public float fuelAmount = 1000f;   

    float emptyWeight = 1f;  // so that mass is always >= 1

    bool flamedOut = false;
    FX effects;

    private Rigidbody2D body;
    
    void Start() {     
        body = GetComponent<Rigidbody2D>();
        effects = GetComponent<FX>();
    }

    void FixedUpdate() {
      
        if(fuelAmount <= 0f)
        {
            FlameOut();
            return;
        }

        // rocket motion computation..
	  
    }

    public void FlameOut()
    {
        if (flamedOut == true)
            return;

        effects.FlameOut();

        if (body.velocity.magnitude < 0.1f)
        {
            flamedOut = true;
            effects.BurnUp();       
        }
    }

}
