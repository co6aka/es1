﻿using UnityEngine;
using System.Collections;

public class FX : MonoBehaviour
{
    GameObject engineFlame;

    public GameObject fire;  //  use as prefab, so it doesn't start up right away
    public AudioClip engine;
    public AudioClip explosion;

    AudioSource audioSrc;
    Quaternion inverse = Quaternion.identity;
    GameObject flag;
    GameObject flames;
    SpriteRenderer sr;

    bool landed = false;
    Rigidbody2D body;

    void Start()
    {
        foreach (Component c in GetComponentsInChildren<Component>())
        {
            if (c.gameObject.name == "jet-flame")
                engineFlame = c.gameObject;
            if (c.gameObject.name == "flag")
                flag = c.gameObject;
        }

        sr = GetComponent<SpriteRenderer>();

        audioSrc = gameObject.AddComponent<AudioSource>();
        audioSrc.PlayOneShot(engine);
        body = GetComponent<Rigidbody2D>();
        inverse.eulerAngles = new Vector3(0f, 0f, 180f);
    }

    public void FlameOut()
    {
        Destroy(engineFlame);
        audioSrc.Stop();
    }

    public void BurnUp()
    {
        if (landed)
            return;

        flag = null;
        StartCoroutine(Rotate());
        Invoke("Inflame", 1.25f);
        Invoke("Charred", 3.25f);
    }

    void Inflame()
    {
        flames = Instantiate(fire);
    }

    void Charred()
    {
        sr.color = new Color(0f, 0f, 0f);
        transform.localScale = new Vector3(1f, 0.8f, 1f);
    }

    void Update()
    {
        if(flames != null)
           flames.transform.position = new Vector2(transform.position.x, transform.position.y - 0.25f);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (landed)
            return;

        if (other.gameObject.name == "moon")
        {
            MoonLanding();          
        }
    }

    public void MoonLanding()
    {
        landed = true;
        print("The Eagle has Landed");
        if (body != null)
            body.velocity = Vector2.zero;
        StartCoroutine(Rotate());
    }

    IEnumerator Rotate()
    {
        if(body != null && flag != null)
          body.isKinematic = true;

        for (int i = 0; i < 180; i++)
        {
            transform.Rotate(0f, 0f, 1f);
            yield return new WaitForSeconds(0.012f);
        }

        if (flag != null)
            StartCoroutine(ShowFlag());
    }

    IEnumerator ShowFlag()
    {
        for (int i = 0; i < 33; i++)
        {
            flag.transform.Translate(0.03f, 0f, 0f);
            yield return new WaitForSeconds(0.014f);
        }
    }
}
